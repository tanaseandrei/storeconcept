import {AppRoutingModule} from './app-routing.module';
import {NgModule} from '@angular/core';
import {HomeComponent} from './components/home/home.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {ItemDetailsComponent} from './components/item-details/item-details.component';
import {MatIconModule} from '@angular/material/icon';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {FormsModule} from '@angular/forms';
import {FirebaseConfig} from '../../firebase.config';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {CheckoutComponent} from './components/checkout/checkout.component';
import {DropzoneDirective} from './directives/dropzone.directive';
import {AngularFireModule} from '@angular/fire';
import {AppComponent} from './app.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';
import {ItemListComponent} from './components/item-list/item-list.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import { ContactComponent } from './components/contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemListComponent,
    ItemDetailsComponent,
    ShoppingCartComponent,
    CheckoutComponent,
    DropzoneDirective,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(FirebaseConfig.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    MatIconModule,
    MatMenuModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
