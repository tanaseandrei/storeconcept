import {Injectable, OnInit} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {BehaviorSubject} from 'rxjs';
import {Item} from '../models/item';
import {Category} from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public itemsSub: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>([]);
  public categorySub: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>([]);
  private selectedItem: Item = null;

  constructor(private firebaseService: FirebaseService) {
    this.firebaseService.getItems().then(itemsQuerySnapshot => {
      const items: Item[] = [];
      itemsQuerySnapshot.forEach(itemQuerySnapshot => {
        items.push({...itemQuerySnapshot.data(), id: itemQuerySnapshot.id} as Item);
      });
      this.itemsSub.next(items);
    });

    // this.getItems();
    this.firebaseService.getCollection('categories').subscribe(ordersQuerySnapshot => {
      const categories: Category[] = [];
      ordersQuerySnapshot.forEach(orderQuerySnapshot => {
        categories.push({...orderQuerySnapshot.data(), id: orderQuerySnapshot.id} as Category);
      });
      this.categorySub.next(categories);
    });
    if (!this.selectedItem) {
      const storageItem = localStorage.getItem('selectedItem');
      this.selectedItem = storageItem ? JSON.parse(storageItem) : null;
    }
  }

  setSelectedItem(item: Item): void {
    localStorage.setItem('selectedItem', JSON.stringify(item));
    this.selectedItem = item;
  }

  getSelectedItem(): Item {
    return this.selectedItem;
  }

  private getItems(): void {
    const localItems = JSON.parse(localStorage.getItem('itemsCache'));
    if (!localItems || this.isExpired(localItems.expiration) || (localItems.list && localItems.list.length === 0)) {
      this.firebaseService.getItems().then(itemsQuerySnapshot => {
        const items: Item[] = [];
        itemsQuerySnapshot.forEach(itemQuerySnapshot => {
          items.push({...itemQuerySnapshot.data(), id: itemQuerySnapshot.id} as Item);
        });
        const maxUpdateItem = items.reduce((prevItem, currentItem) => currentItem.updatedAt > prevItem.updatedAt ? currentItem : prevItem);
        const cacheItems = {
          list: items,
          maxUpdatedAt: maxUpdateItem.updatedAt,
          expiration: Date.now() + (3600 * 1000)
        };
        this.itemsSub.next(items);
        localStorage.setItem('itemsCache', JSON.stringify(cacheItems));
      });
    } else {
      const cacheItems = JSON.parse(localStorage.getItem('itemsCache'));
      const items = cacheItems.list;
      this.firebaseService.getItemsMaxUpdatedAt(parseInt(cacheItems.maxUpdatedAt, 10)).then(itemQuerySnap => {
        const updatedItems = [];
        itemQuerySnap.forEach(item => {
          updatedItems.push({...item.data(), id: item.id});
        });
        console.log('UPDATED ITEMS: ', updatedItems);
        updatedItems.forEach(updatedItem => {
          const indexToUpdate = items.findIndex(item => item.id === updatedItem.id);
          if (indexToUpdate >= 0) {
            items.splice(indexToUpdate, 1);
          }
          if (updatedItem.active) {
            items.push(updatedItem);
          }
        });

        // Set New Cache Items
        const maxUpdateItem = items.reduce((prevItem, currentItem) => currentItem.updatedAt > prevItem.updatedAt ? currentItem : prevItem);
        cacheItems.maxUpdatedAt = maxUpdateItem.updatedAt;
        cacheItems.list = items;
        cacheItems.expiration = Date.now() + (3600 * 1000);
        localStorage.setItem('itemsCache', JSON.stringify(cacheItems));
      });
      this.itemsSub.next(items);
    }
  }

  private isExpired(expiration: number): boolean {
    return Date.now() > expiration;
  }
}
