import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {Item} from '../models/item';
import {Observable} from 'rxjs';
import {firestore} from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    public afAuth: AngularFireAuth,
    private db: AngularFirestore
  ) {
    this.afAuth.signInAnonymously();
  }

  getCollection(collectionName: string): any {
    return this.db.collection(collectionName).get();
  }

  getItems(): any {
    return this.db.collection('items').ref
      .where('active', '==', true)
      .get();
  }

  getItemsMaxUpdatedAt(maxUpdatedAt: number): Promise<any> {
    return this.db.collection('items').ref
      .where('updatedAt', '>', maxUpdatedAt).get();
  }
}
