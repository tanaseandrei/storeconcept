import { Injectable } from '@angular/core';
import {CartItem} from '../models/cart-item';
import {Item} from '../models/item';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  cartItems: CartItem[] = [];
  public cartItemNumberSub: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor() { }

  addToCart(item: Item, quantity = 1): void {
    let cartItem: CartItem;
    const cartItemNumber = this.cartItemNumberSub.getValue();
    if (this.cartItems && this.cartItems.length > 0) {
      cartItem = this.cartItems.find($cartItem => $cartItem.itemId === item.id);
    }
    if (cartItem) {
      cartItem.quantity = cartItem.quantity + quantity;
      cartItem.totalPrice = cartItem.totalPrice + item.price;
    } else {
      this.cartItems.push({
        itemId: item.id,
        quantity,
        priceId: item.stripePriceId,
        totalPrice: item.price
      });
    }

    this.cartItemNumberSub.next(cartItemNumber + 1);
  }
}
