export interface Item {
  id: string;
  name: string;
  description: string;
  picture: string;
  price: number;
  stripePriceId: string;
  stripePricesList: string[];
  category: string;
  updatedAt?: string;
  deletedAt?: string;
}
