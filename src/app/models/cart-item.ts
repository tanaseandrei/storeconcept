export interface CartItem {
  itemId: string;
  priceId: string;
  quantity: number;
  totalPrice: number;
}
