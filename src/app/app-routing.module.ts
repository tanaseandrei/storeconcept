import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ItemDetailsComponent} from './components/item-details/item-details.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  { path: 'home', component:  HomeComponent},
  { path: 'details', pathMatch: 'full', component:  ItemDetailsComponent},
  { path: 'cart', pathMatch: 'full', component:  ShoppingCartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
