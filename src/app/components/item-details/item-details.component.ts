import {Component, OnInit} from '@angular/core';
import {Item} from '../../models/item';
import {DataService} from '../../services/data.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {
  public item: Item;
  public similarItems: Item[];

  constructor(
    private dataService: DataService,
    private router: Router,
    private shoppingCartService: ShoppingCartService
    ) { }

  ngOnInit(): void {
    this.item = this.dataService.getSelectedItem();
    this.dataService.itemsSub.subscribe(items => {
      if (items) {
        this.similarItems = [];
        items.forEach(item => {
          if (this.similarItems.length > 5) {
            return;
          }
          if (item.category === this.item.category && item.id !== this.item.id) {
            this.similarItems.push(item);
          }
        });
      }
    });
    this.similarItems = [];
    this.dataService.itemsSub.getValue()?.forEach(item => {
      if (this.similarItems.length > 5) {
        return;
      }
      if (item.category === this.item.category && item.id !== this.item.id) {
        this.similarItems.push(item);
      }
    });
  }

  addToCart(item: Item): void {
    this.shoppingCartService.addToCart(item, 1);
  }

  setNewItem(newItem: Item): void {
    this.dataService.setSelectedItem(newItem);
    this.item = newItem;
    this.similarItems = [];
    this.dataService.itemsSub.getValue()?.forEach(item => {
      if (this.similarItems.length > 5) {
        return;
      }
      if (item.category === this.item.category && item.id !== this.item.id) {
        this.similarItems.push(item);
      }
    });
  }
}
