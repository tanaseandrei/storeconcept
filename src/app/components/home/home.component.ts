import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { disableBodyScroll } from 'body-scroll-lock';
import { BodyScrollOptions } from 'body-scroll-lock';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  constructor() {
  }

  @ViewChild('componentContent') componentContent;

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const options: BodyScrollOptions = {
      reserveScrollBarGap: true,
    };
    disableBodyScroll(this.componentContent.nativeElement, options);
  }
}
