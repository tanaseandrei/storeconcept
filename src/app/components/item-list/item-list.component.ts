import { Component, OnInit } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Item} from '../../models/item';
import {Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {Category} from '../../models/category';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  items: Item[] = [];
  itemList: Item[] = [];
  categories: Category[] = [];
  selectedCategory: string;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.items = this.dataService.itemsSub.getValue();
    this.itemList = this.items;
    this.dataService.itemsSub.subscribe((items: Item[]) => {
      this.items = items;
      this.itemList = this.items;
    });

    this.categories = this.dataService.categorySub.getValue();
    this.dataService.categorySub.subscribe((categories: Category[]) => this.categories = categories);
  }

  goToItemDetails(item: Item): void {
    this.dataService.setSelectedItem(item);
    this.router.navigateByUrl('/details', {queryParams: {item}});
  }

  onSelectCategory(id: string): void {
    if (this.selectedCategory === id) {
      this.selectedCategory = null;
      this.itemList = this.items;
      return;
    }
    this.selectedCategory = id;
    this.itemList = this.items.filter(item => item.category === this.selectedCategory);
  }

  applyFilter(event: Event): void{
    const filterValue = (event.target as HTMLInputElement).value;
    this.itemList = this.items.filter(item => {
      if (this.selectedCategory) {
        return item.category === this.selectedCategory &&
          (filterValue ? item.name.toLowerCase().includes(filterValue.toLowerCase()) : true);
      } else {
        return filterValue ? item.name.toLowerCase().includes(filterValue.toLowerCase()) : true;
      }
    });
  }
}
