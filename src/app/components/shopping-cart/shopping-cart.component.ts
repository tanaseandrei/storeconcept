import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {Item} from '../../models/item';
import {CartItem} from '../../models/cart-item';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {loadStripe, RedirectToCheckoutClientOptions} from '@stripe/stripe-js';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  public items: Item[];
  public cartItems: CartItem[];
  public stripe: any;

  constructor(
    private dataService: DataService,
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.items = this.dataService.itemsSub.getValue();
    loadStripe(environment.stripe).then(result => {
      this.stripe = result;
    });
    this.dataService.itemsSub.subscribe((items: Item[]) => {
      this.items = items;
    });

    this.cartItems = this.shoppingCartService.cartItems;

  }

  getCartItem(cartItem: CartItem): Item {
    return this.items ? this.items.find(item => item.id === cartItem.itemId) : null;
  }

  removeWholeItem(cartItem: CartItem): void {
    const cartItemsNumber = this.shoppingCartService.cartItemNumberSub.getValue();
    this.shoppingCartService.cartItemNumberSub.next(cartItemsNumber - cartItem.quantity);
    this.cartItems.splice(this.cartItems.indexOf(cartItem), 1);
  }

  removeItem(cartItem: CartItem): void {
    const cartItemsNumber = this.shoppingCartService.cartItemNumberSub.getValue();
    this.shoppingCartService.cartItemNumberSub.next(cartItemsNumber - 1);
    if (cartItem.quantity === 1) {
      this.cartItems.splice(this.cartItems.indexOf(cartItem), 1);
      return;
    }
    if (cartItem.quantity > 0) {
      cartItem.quantity = cartItem.quantity - 1;
      cartItem.totalPrice = cartItem.totalPrice - this.getCartItem(cartItem).price;
    }
  }

  addItem(cartItem: CartItem): void {
    const cartItemsNumber = this.shoppingCartService.cartItemNumberSub.getValue();
    this.shoppingCartService.cartItemNumberSub.next(cartItemsNumber + 1);
    cartItem.quantity = cartItem.quantity + 1;
    cartItem.totalPrice = cartItem.totalPrice + this.getCartItem(cartItem).price;
  }

  getTotal(): number {
    let total = 0;
    this.cartItems.forEach(cartItem => {
      const item = this.getCartItem(cartItem);
      if (item) {
        total = total + (item.price * cartItem.quantity);
      }
    });
    return total;
  }

  goToCheckout(): void {
    const lineItems: {price: string, quantity: number}[] = [];
    this.cartItems.forEach(cartItem => {
      lineItems.push({
        price: cartItem.priceId,
        quantity: cartItem.quantity
      });
    });

    const options: RedirectToCheckoutClientOptions = {
      lineItems,
      mode: 'payment',
      successUrl: environment.baseUrl + '/cart',
      cancelUrl: environment.baseUrl + '/cart',
      shippingAddressCollection: {allowedCountries: ['RO']},
      clientReferenceId: 'testId'
    };
    this.stripe.redirectToCheckout(options);
  }


  goToHome(): void {
    this.router.navigateByUrl('/home');
  }
}
