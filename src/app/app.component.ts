import { Component } from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {ShoppingCartService} from './services/shopping-cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public itemNumber = 0;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private shopingCartService: ShoppingCartService
  ) {
    this.matIconRegistry.addSvgIcon(
      'store',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/local_grocery_store.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'cart',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/cart.svg')
    );

    this.shopingCartService.cartItemNumberSub.subscribe(itemNumber => this.itemNumber = itemNumber);
  }

  title = 'store-concept';

  goToCart(): void {
    this.router.navigateByUrl('/cart');
  }

  goToHome(): void {
    this.router.navigateByUrl('/home');
  }
}
