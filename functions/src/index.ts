import {EventContext} from 'firebase-functions';
import {QueryDocumentSnapshot} from 'firebase-functions/lib/providers/firestore';
import Stripe from 'stripe';
import * as firebaseAdmin from 'firebase-admin';
import Firestore = firebaseAdmin.firestore.Firestore;

const functions = require('firebase-functions');
const stripeWebhook = require('stripe')(functions.config().keys.all);
const stripe = new Stripe(functions.config().keys.signing, {apiVersion: '2020-03-02'});
const admin = require('firebase-admin');

const checkoutCompletedSecret = functions.config().keys.webhooks.checkout.complete;

admin.initializeApp();
const dbFirestore: Firestore = admin.firestore();

exports.checkoutCompleted = functions.https.onRequest((request: any, response: any) => {
  const sig = request.headers['stripe-signature'];
  let event: any;

  try {
    event = stripeWebhook.webhooks.constructEvent(request.rawBody, sig, checkoutCompletedSecret);
  } catch (err) {
    return response.status(400).end();
  }
  // Handle the checkout.session.completed event
  if (event.type === 'checkout.session.completed') {
    const session = event.data.object;
    console.log('Session ID: ', session.id);
    stripe.checkout.sessions.listLineItems(session.id, {limit: 20}).then((lineItems: any) => {
      const prices: {id: number, quantity: number}[] = [];
      lineItems.data.forEach((item: any) => {
        prices.push({id: item.price.id, quantity: item.quantity});
      });
      dbFirestore.collection('orders').doc().set({
        name: session.shipping.name,
        address: session.shipping.address,
        paymentIntent: session.payment_intent,
        customerId: session.customer,
        amount: session.amount_total,
        items: {
          prices,
          hasMore: lineItems.has_more,
          lastLineItemId: lineItems.data ? lineItems.data[lineItems.data.length - 1].id : null
        },
        status: 'Payment'
      }).then((ref: any) => {
        console.log('REF: ', ref);
        response.json({received: true});
      }).catch((error: any) => {
        console.log('ERROR firestore: ', error);
      });
    }).catch((error: any) => console.log('ERROR firestore: ', error));
  }
});

exports.getPaymentIntent = functions.https.onRequest((request: any, response: any) => {
stripe.paymentIntents.retrieve('pi_1H9HKkAoXWpQuKt7a0UeUCa1').then(result => {
  response.json(result);
}).catch((error: any) => console.log('ERROR firestore: ', error));

});

exports.createNewProduct = functions.firestore.document('items/{itemId}').onCreate((snapshot: QueryDocumentSnapshot, context: EventContext) => {
  const item = snapshot.data();

  return stripe.products.create({
    name: item.name,
    description: item.description,
    images: [item.picture],
    type: 'good'
  }).then(product => {
    return stripe.prices.create({
      product: product.id,
      unit_amount: item.price * 100,
      currency: 'eur'
    }).then(price => {
      console.log('Saving stripe product!');
      return dbFirestore.collection('items').doc(snapshot.id).set(
        {
          stripeProductId: product.id,
          stripePriceId: price.id
        }, { merge: true })
        .then((result: any) => {
          console.log('result: ', result);
          return 'Save Success';
        })
        .catch((error: any) => {
          console.log('ERROR firestore: ', error);
          return;
        });
    })
      .catch(error => {
        console.log('ERROR: ', error);
        return dbFirestore.collection('items').doc(snapshot.id).set(
          {
            stripePriceId: 'error'
          }, { merge: true })
          .then((result: any) => console.log('result: ', result))
          .catch((firestoreError: any) => {
            console.log('ERROR firestore: ', firestoreError);
            return;
          });
      });
  })
    .catch(error => {
      console.log('ERROR: ', error);
      return dbFirestore.collection('items').doc(snapshot.id).set(
        {
          stripeProductId: 'error'
        }, { merge: true })
        .then((result: any) => {
          console.log('result: ', result);
          return;
        })
        .catch((firestoreError: any) => console.log('ERROR firestore: ', firestoreError));
    });
});
